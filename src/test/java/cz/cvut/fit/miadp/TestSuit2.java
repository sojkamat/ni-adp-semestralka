package cz.cvut.fit.miadp;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith( Suite.class )

@Suite.SuiteClasses( {
        MovingStrategyTest.class,
        CannonCmdsUndoTest.class,
        InitCheckTest.class,
        MockTest.class

})

public class TestSuit2 { }
