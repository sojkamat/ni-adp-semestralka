package cz.cvut.fit.miadp;

import cz.cvut.fit.miadp.mvcgame.command.MoveCannonUpCmd;
import cz.cvut.fit.miadp.mvcgame.command.toggleMovingStrategyCmd;
import cz.cvut.fit.miadp.mvcgame.config.MvcGameConfig;
import cz.cvut.fit.miadp.mvcgame.model.GameModel;
import cz.cvut.fit.miadp.mvcgame.model.IGameModel;
import cz.cvut.fit.miadp.mvcgame.state.IShootingMode;
import cz.cvut.fit.miadp.mvcgame.strategy.IMovingStrategy;
import cz.cvut.fit.miadp.mvcgame.strategy.SimpleMovingStrategy;
import org.junit.Test;
import junit.framework.Assert;

public class MovingStrategyTest {

    @Test
    public void changeMovingStrategy( ){
        IGameModel model = new GameModel( );

        IMovingStrategy initStrategy = model.getMovingStrategy();
        model.registerCommand( new MoveCannonUpCmd( model ) );
        model.registerCommand( new MoveCannonUpCmd( model ) );
        model.registerCommand( new MoveCannonUpCmd( model ) );
        model.update();
        IMovingStrategy unchangedStrategy = model.getMovingStrategy();
        model.registerCommand( new toggleMovingStrategyCmd( model ) );
        model.update();
        IMovingStrategy nextStrategy = model.getMovingStrategy();
        model.registerCommand( new toggleMovingStrategyCmd( model ) );
        model.update();
        IMovingStrategy previousStrategy = model.getMovingStrategy();

        Assert.assertEquals("SimpleMovingStrategy", initStrategy.getStrategyName());
        Assert.assertEquals("SimpleMovingStrategy", unchangedStrategy.getStrategyName());
        Assert.assertEquals("RealisticMovingStrategy", nextStrategy.getStrategyName());
        Assert.assertEquals("SimpleMovingStrategy", previousStrategy.getStrategyName());

    }
}
