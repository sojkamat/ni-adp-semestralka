package cz.cvut.fit.miadp;

import cz.cvut.fit.miadp.mvcgame.config.MvcGameConfig;
import cz.cvut.fit.miadp.mvcgame.model.GameModel;
import cz.cvut.fit.miadp.mvcgame.model.IGameModel;
import cz.cvut.fit.miadp.mvcgame.model.Position;
import cz.cvut.fit.miadp.mvcgame.strategy.IMovingStrategy;
import org.junit.Assert;
import org.junit.Test;

public class InitCheckTest {

    @Test
    public void checkInitState( ) {
        IGameModel model = new GameModel();
        for (int i = 0; i < 20; i++)
            model.update();
        int enemyCount = model.getEnemyCount();
        Position cannon = model.getCannonPosition();
        IMovingStrategy initStrategy = model.getMovingStrategy();

        Assert.assertEquals(MvcGameConfig.MAX_ENEMIES, enemyCount);
        Assert.assertEquals(MvcGameConfig.CANNON_POS_X, cannon.getX());
        Assert.assertEquals(MvcGameConfig.CANNON_POS_Y, cannon.getY());
        Assert.assertEquals("SimpleMovingStrategy", model.getMovingStrategy().getStrategyName());
    }
}
