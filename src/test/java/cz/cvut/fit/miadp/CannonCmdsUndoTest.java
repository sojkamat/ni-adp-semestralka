package cz.cvut.fit.miadp;

import cz.cvut.fit.miadp.mvcgame.command.AimCannonUpCmd;
import cz.cvut.fit.miadp.mvcgame.command.CannonPowerDownCmd;
import cz.cvut.fit.miadp.mvcgame.command.CannonPowerUpCmd;
import cz.cvut.fit.miadp.mvcgame.command.MoveCannonUpCmd;
import cz.cvut.fit.miadp.mvcgame.config.MvcGameConfig;
import cz.cvut.fit.miadp.mvcgame.model.GameModel;
import cz.cvut.fit.miadp.mvcgame.model.IGameModel;
import cz.cvut.fit.miadp.mvcgame.state.IShootingMode;
import junit.framework.Assert;
import org.junit.Test;

public class CannonCmdsUndoTest {

    @Test
    public void undoCannonCommands( ) {
        IGameModel model = new GameModel();
        int initPositionX = model.getCannonPosition( ).getX( );
        int initPositionY = model.getCannonPosition( ).getY( );
        int initPower = model.getCannonPower();
        double initAngle = model.getCannonAngle();
        model.registerCommand( new MoveCannonUpCmd( model ) );
        model.registerCommand( new MoveCannonUpCmd( model ) );
        model.registerCommand( new CannonPowerUpCmd( model ) );
        model.registerCommand( new AimCannonUpCmd( model ) );
        model.update( );
        int nextPositionX = model.getCannonPosition( ).getX( );
        int nextPositionY = model.getCannonPosition( ).getY( );
        int nextPower = model.getCannonPower();
        double nextAngle = model.getCannonAngle();
        model.undoLastCommand( );
        model.undoLastCommand( );
        model.undoLastCommand( );
        model.undoLastCommand( );
        model.update( );
        int previousPositionX = model.getCannonPosition( ).getX( );
        int previousPositionY = model.getCannonPosition( ).getY( );
        int previousPower = model.getCannonPower();
        double previousAngle = model.getCannonAngle();

        Assert.assertEquals(MvcGameConfig.CANNON_POS_X, initPositionX);
        Assert.assertEquals(MvcGameConfig.CANNON_POS_Y, initPositionY);
        Assert.assertEquals(MvcGameConfig.CANNON_POS_X , nextPositionX);
        Assert.assertEquals(MvcGameConfig.CANNON_POS_Y - 2*MvcGameConfig.MOVE_STEP, nextPositionY);
        Assert.assertEquals(MvcGameConfig.CANNON_POS_X, previousPositionX);
        Assert.assertEquals(MvcGameConfig.CANNON_POS_Y, previousPositionY);

        Assert.assertEquals(MvcGameConfig.INIT_POWER, initPower);
        Assert.assertEquals(MvcGameConfig.INIT_POWER + MvcGameConfig.POWER_STEP, nextPower);
        Assert.assertEquals(MvcGameConfig.INIT_POWER, previousPower);

        Assert.assertEquals((double)MvcGameConfig.INIT_ANGLE, initAngle);
        Assert.assertEquals((double)MvcGameConfig.INIT_ANGLE - MvcGameConfig.ANGLE_STEP, nextAngle);
        Assert.assertEquals((double)MvcGameConfig.INIT_ANGLE, previousAngle);

    }
}
