package cz.cvut.fit.miadp;

import cz.cvut.fit.miadp.mvcgame.abstractFactory.GameObjectsFactoryA;
import cz.cvut.fit.miadp.mvcgame.abstractFactory.IGameObjectsFactory;
import cz.cvut.fit.miadp.mvcgame.model.GameModel;
import cz.cvut.fit.miadp.mvcgame.model.IGameModel;
import cz.cvut.fit.miadp.mvcgame.model.Position;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.AbsEnemy;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.AbsMissile;
import cz.cvut.fit.miadp.mvcgame.strategy.SimpleMovingStrategy;
import junit.framework.Assert;
import org.junit.Test;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class MockTest {

    @Test
    public void hitEnemy() {
        IGameModel model = mock( GameModel.class );

        IGameObjectsFactory goFact = new GameObjectsFactoryA( model );
        AbsEnemy enemy = goFact.createEnemy();
        model.addEnemy(enemy);
        Position enemyPosition = enemy.getPosition();

        when( model.getCannonPosition( ) ).thenReturn( enemyPosition );
        AbsMissile missile = goFact.createMissile( 0 , 1000 );
        model.destroyEnemies();
        Assert.assertEquals(0, model.getEnemyCount());

    }
}
