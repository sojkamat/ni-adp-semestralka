package cz.cvut.fit.miadp.mvcgame.model.gameObjects.familyB;

import cz.cvut.fit.miadp.mvcgame.abstractFactory.IGameObjectsFactory;
import cz.cvut.fit.miadp.mvcgame.model.Position;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.AbsStaticText;

public class DiscourageText extends AbsStaticText {

    private IGameObjectsFactory goFact;

    public DiscourageText(Position initialPosition, IGameObjectsFactory goFact) {
        this.position = initialPosition;
        this.goFact = goFact;
        this.text = "Boooooo!";
    }

    @Override
    public String getText() {
        return this.text;
    }
}
