package cz.cvut.fit.miadp.mvcgame.model;

import java.util.List;

import cz.cvut.fit.miadp.mvcgame.command.AbstractGameCommand;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.AbsEnemy;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.GameObject;
import cz.cvut.fit.miadp.mvcgame.observer.IObservable;
import cz.cvut.fit.miadp.mvcgame.strategy.IMovingStrategy;

public interface IGameModel extends IObservable {
    public void moveCannonUp();
    public void moveCannonDown();
    public void cannonShoot();
    public void aimCannonUp();
    public void aimCannonDown();
    public void cannonPowerUp();
    public void cannonPowerDown();
    public void toggleMovingStrategy();
    public void toggleShootingMode( );
    public void changeText();

    public void update();
    public List<GameObject> getGameObjects();
    public IMovingStrategy getMovingStrategy();
    public Object createMemento();
    public void setMemento(Object memento);
    public void destroyEnemies( );
    public void addEnemy(AbsEnemy enemy);

	public Position getCannonPosition();
    public int getCannonPower();
    public double getCannonAngle();
    public int getScore();
    public int getEnemyCount();

    public void registerCommand( AbstractGameCommand cmd );
    public void undoLastCommand( );
}
