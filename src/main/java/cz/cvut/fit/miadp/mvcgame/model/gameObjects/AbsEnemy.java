package cz.cvut.fit.miadp.mvcgame.model.gameObjects;

import cz.cvut.fit.miadp.mvcgame.visitor.IVisitor;

public abstract class AbsEnemy extends GameObject{

    protected Boolean destroyed;
    protected int points;

    @Override
    public void acceptVisitor( IVisitor visitor ){
        visitor.visitEnemy( this );
    }

    public int getPoints() {
        return points;
    }
}
