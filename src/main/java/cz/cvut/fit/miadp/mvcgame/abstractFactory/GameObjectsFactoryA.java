package cz.cvut.fit.miadp.mvcgame.abstractFactory;

import cz.cvut.fit.miadp.mvcgame.config.MvcGameConfig;
import cz.cvut.fit.miadp.mvcgame.model.IGameModel;
import cz.cvut.fit.miadp.mvcgame.model.Position;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.AbsStaticText;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.familyA.*;

public class GameObjectsFactoryA implements IGameObjectsFactory {

    private IGameModel model;

    public GameObjectsFactoryA( IGameModel model ){
        this.model = model;
    }


    @Override
    public CannonA createCannon( ) {
        return new CannonA( new Position( MvcGameConfig.CANNON_POS_X, MvcGameConfig.CANNON_POS_Y ), this );
    }

    @Override
    public MissileA createMissile( double initAngle, int initVelocity ) {
        return new MissileA(
                new Position(
                        model.getCannonPosition().getX(),
                        model.getCannonPosition().getY()
                ),
                initAngle,
                initVelocity,
                this.model.getMovingStrategy()
        );
    }

    @Override
    public EnemyA createEnemy( ) {
        return new EnemyA( new Position( rng(MvcGameConfig.CANNON_POS_X + 100, MvcGameConfig.MAX_X-50),
                                         rng(50, MvcGameConfig.MAX_Y-50) ), this );
    }

    public int rng(int min, int max) {
        return (int) ((Math.random() * (max - min)) + min);
    }


    @Override
    public GameinfoA createGameinfo( ) {
        return new GameinfoA( new Position( MvcGameConfig.GAMEINFO_POS_X, MvcGameConfig.GAMEINFO_POS_Y ), this );
    }

    @Override
    public EncourageText createStaticText() {
        return new EncourageText( new Position(MvcGameConfig.MAX_X/2, MvcGameConfig.MAX_Y-20), this);
    }

}


