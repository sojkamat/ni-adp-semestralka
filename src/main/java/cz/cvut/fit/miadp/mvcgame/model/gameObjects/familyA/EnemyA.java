package cz.cvut.fit.miadp.mvcgame.model.gameObjects.familyA;

import cz.cvut.fit.miadp.mvcgame.abstractFactory.IGameObjectsFactory;
import cz.cvut.fit.miadp.mvcgame.model.Position;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.AbsEnemy;
import cz.cvut.fit.miadp.mvcgame.visitor.IVisitor;

public class EnemyA extends AbsEnemy {

    private IGameObjectsFactory goFact;

    public EnemyA(Position initialPosition, IGameObjectsFactory goFact ) {
        this.position = initialPosition;
        this.goFact = goFact;
        this.destroyed = Boolean.FALSE;
        this.points = 1;
    }

}
