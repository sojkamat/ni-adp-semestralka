package cz.cvut.fit.miadp.mvcgame.command;

import cz.cvut.fit.miadp.mvcgame.model.IGameModel;

public class toggleShootingModeCmd extends AbstractGameCommand {

    public toggleShootingModeCmd( IGameModel subject ){
        this.subject = subject;
    }

    @Override
    protected void execute( ) {
        this.subject.toggleShootingMode();
    }
}
