package cz.cvut.fit.miadp.mvcgame.decorator;

public interface TextInterface {
    String getText();
}
