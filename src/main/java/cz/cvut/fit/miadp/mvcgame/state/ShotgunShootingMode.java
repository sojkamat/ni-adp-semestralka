package cz.cvut.fit.miadp.mvcgame.state;

import cz.cvut.fit.miadp.mvcgame.model.gameObjects.AbsCannon;

public class ShotgunShootingMode implements IShootingMode{
    @Override
    public String getName() {
        return "ShotgunShootingMode";
    }

    @Override
    public void shoot(AbsCannon cannon) {
        cannon.primitiveShoot( );
        cannon.aimUp( );
        cannon.primitiveShoot( );
        cannon.aimUp( );
        cannon.primitiveShoot( );
        cannon.aimDown( );
        cannon.aimDown( );
        cannon.aimDown( );
        cannon.primitiveShoot( );
        cannon.aimDown( );
        cannon.primitiveShoot( );
        cannon.aimUp( );
        cannon.aimUp( );
    }
}
