package cz.cvut.fit.miadp.mvcgame.model;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Queue;
import java.util.Stack;
import java.util.concurrent.LinkedBlockingQueue;

import cz.cvut.fit.miadp.mvcgame.abstractFactory.GameObjectsFactoryA;
import cz.cvut.fit.miadp.mvcgame.abstractFactory.GameObjectsFactoryB;
import cz.cvut.fit.miadp.mvcgame.abstractFactory.IGameObjectsFactory;
import cz.cvut.fit.miadp.mvcgame.command.AbstractGameCommand;
import cz.cvut.fit.miadp.mvcgame.config.MvcGameConfig;
import cz.cvut.fit.miadp.mvcgame.decorator.BadText1;
import cz.cvut.fit.miadp.mvcgame.decorator.GoodText1;
import cz.cvut.fit.miadp.mvcgame.decorator.NeutralText1;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.*;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.familyA.EncourageText;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.familyB.DiscourageText;
import cz.cvut.fit.miadp.mvcgame.observer.IObserver;
import cz.cvut.fit.miadp.mvcgame.state.IShootingMode;
import cz.cvut.fit.miadp.mvcgame.state.SingleShootingMode;
import cz.cvut.fit.miadp.mvcgame.strategy.IMovingStrategy;
import cz.cvut.fit.miadp.mvcgame.strategy.RealisticMovingStrategy;
import cz.cvut.fit.miadp.mvcgame.strategy.SimpleMovingStrategy;


public class GameModel implements IGameModel {

    private AbsCannon cannon;
    private List<AbsMissile> missiles;
    private List<IObserver> observers;
    private IGameObjectsFactory goFact;
    private IGameObjectsFactory goFactB;
    private List<AbsEnemy> enemies;
    private AbsGameinfo gameinfo;
    private AbsStaticText text;
    private LocalDateTime time;
    //private IMovingStrategy movingStrategy;

    private Queue<AbstractGameCommand> unexecuteCmds = new LinkedBlockingQueue<AbstractGameCommand>( );
    private Stack<AbstractGameCommand> executedCmds = new Stack<AbstractGameCommand>();

    public GameModel( ){
        this.goFact = new GameObjectsFactoryA( this );
        this.goFactB = new GameObjectsFactoryB( this );
        this.cannon = this.goFact.createCannon( );
        this.missiles = new ArrayList<AbsMissile>();
        this.observers = new ArrayList<IObserver>();
        this.enemies = new ArrayList<AbsEnemy>();
        this.gameinfo = this.goFact.createGameinfo();
        this.text = this.goFact.createStaticText();
        this.time = LocalDateTime.now();

        this.gameinfo.setScore(0);
        this.gameinfo.setMovingStrategy(new SimpleMovingStrategy());
        this.gameinfo.setShootingMode(this.cannon.getShootingMode());
        this.gameinfo.setCannonAngle(MvcGameConfig.INIT_ANGLE);
        this.gameinfo.setCannonPower(MvcGameConfig.INIT_POWER);
    }

    public Position getCannonPosition( ){
        return this.cannon.getPosition( );
    }
    public int getCannonPower() { return this.gameinfo.getCannonPower();}
    public double getCannonAngle() { return this.gameinfo.getCannonAngle();}
    public int getScore() {return this.gameinfo.getScore();}
    public List<AbsEnemy> getEnemies() {return this.enemies;}
    public void setEnemies(List<AbsEnemy> enemies) {this.enemies = enemies;}

    public void moveCannonDown() {
        this.cannon.moveDown();
        this.notifyObservers( );
    }

    public void moveCannonUp() {
        this.cannon.moveUp();
        this.notifyObservers( );
    }

    public void aimCannonUp( ){
        this.cannon.aimUp( );
        this.gameinfo.setCannonAngle(this.gameinfo.getCannonAngle() - MvcGameConfig.ANGLE_STEP);
        this.notifyObservers( );
    }

    public void aimCannonDown( ){
        this.cannon.aimDown( );
        this.gameinfo.setCannonAngle(this.gameinfo.getCannonAngle() + MvcGameConfig.ANGLE_STEP);
        this.notifyObservers( );
    }

    public void cannonPowerUp( ){
        this.cannon.powerUp( );
        this.gameinfo.setCannonPower(this.gameinfo.getCannonPower() + MvcGameConfig.POWER_STEP);
        this.notifyObservers( );
    }

    public void cannonPowerDown( ){
        this.cannon.powerDown( );
        this.gameinfo.setCannonPower(Math.max(this.gameinfo.getCannonPower() - MvcGameConfig.POWER_STEP, 1));
        this.notifyObservers( );
    }

    public void update( ) {
        this.executeCmds( );
        this.moveMissiles( );
        this.checkEnemyCount();
        //this.checkText();
    }

    private void executeCmds( ) {
        while( !this.unexecuteCmds.isEmpty( ) ){
            AbstractGameCommand cmd = this.unexecuteCmds.poll( );
            cmd.doExecute( );
            this.executedCmds.push( cmd );
        }
    }

    private void moveMissiles( ){
        for( AbsMissile missile : this.missiles ){
            missile.move( );
        }
        this.destroyMissiles();
        this.notifyObservers( );
    }

    private void destroyMissiles( ){
        List<AbsMissile> toRemove = new ArrayList<AbsMissile>();
        for( AbsMissile missile : this.missiles ){
            if ( missile.getPosition( ).getX( ) > MvcGameConfig.MAX_X ){
                toRemove.add( missile );
            }
        }
        this.missiles.removeAll( toRemove );
    }

    private Boolean objectsCollide(GameObject obj1, GameObject obj2) {
        if (   obj1.getPosition().getX() - MvcGameConfig.HITBOX <= obj2.getPosition().getX()
            && obj1.getPosition().getX() + MvcGameConfig.HITBOX >= obj2.getPosition().getX()
            && obj1.getPosition().getY() - MvcGameConfig.HITBOX <= obj2.getPosition().getY()
            && obj1.getPosition().getY() + MvcGameConfig.HITBOX >= obj2.getPosition().getY() )
        {
            return Boolean.TRUE;
        }
        return Boolean.FALSE;
    }


    public void destroyEnemies( ){
        List<AbsEnemy> toRemove = new ArrayList<AbsEnemy>();
        for( AbsEnemy enemy : this.enemies ) {
            for( AbsMissile missile : this.missiles ) {
                if ( objectsCollide(enemy, missile) ){
                    toRemove.add( enemy );
                    this.gameinfo.setScore(this.gameinfo.getScore() + enemy.getPoints());
                }
            }
        }
        this.enemies.removeAll( toRemove );
    }

    private void checkEnemyCount() {
        if ( enemies.size() < MvcGameConfig.MAX_ENEMIES ) {
            double pst = Math.random();
            AbsEnemy enemy = null;
            if (pst >= 0.3) {
                enemy = this.goFact.createEnemy();
                enemies.add(enemy);
            }
            else {
                enemy = this.goFactB.createEnemy();
                enemies.add(enemy);
            }
        }
        this.destroyEnemies();
        this.notifyObservers();
    }

    public void addEnemy(AbsEnemy enemy) {
        this.enemies.add(enemy);
    }

    public int getEnemyCount() {return this.enemies.size();}

    public void checkText() {
        this.time = LocalDateTime.now();
        int s = this.time.getSecond();
        int n = this.time.getNano();
        if (s % 10 == 0 && n % 5000 == 0) {
            changeText();
        }
//        this.notifyObservers();
    }

    public void changeText() {
        double pst = Math.random();
        if (pst <= 0.5) {
            this.text = this.goFact.createStaticText();
            if (this.text instanceof EncourageText && pst < 0.35) {
                GoodText1 txt = new GoodText1(this.text);
                this.text.setText(txt.getText());
                if (pst < 0.2) {
                    NeutralText1 txt2 = new NeutralText1(this.text);
                    this.text.setText(txt2.getText());
                }
            }
        }
        else {
            this.text = this.goFactB.createStaticText();
            if (this.text instanceof DiscourageText && pst < 0.8) {
                BadText1 txt = new BadText1(this.text);
                this.text.setText(txt.getText());
                if (pst < 0.65) {
                    NeutralText1 txt2 = new NeutralText1(this.text);
                    this.text.setText(txt2.getText());
                }
            }
        }
        this.notifyObservers();
    }

    @Override
    public void registerObserver(IObserver obs) {
        if( !this.observers.contains( obs ) ){
            this.observers.add( obs );
        }
    }

    @Override
    public void unregisterObserver(IObserver obs) {
        if( this.observers.contains( obs ) ){
            this.observers.remove( obs );
        }
    }

    @Override
    public void notifyObservers() {
        for( IObserver obs: this.observers){
            obs.update( );
        }
    }

    public void cannonShoot( ){
        this.missiles.addAll( cannon.shoot( ) );
        this.notifyObservers( );
    }

    public List<AbsMissile> getMissiles( ) {
        return this.missiles;
    }

    public List<GameObject> getGameObjects() {
        List<GameObject> go = new ArrayList<GameObject>();
        go.addAll( this.missiles );
        go.addAll( this.enemies );
        go.add( this.cannon );
        go.add( this.gameinfo );
        go.add( this.text );
        return go;
    }

    public IMovingStrategy getMovingStrategy( ){
        return this.gameinfo.getMovingStrategy();
    }

    public void toggleMovingStrategy( ){
        if ( this.gameinfo.getMovingStrategy() instanceof SimpleMovingStrategy ){
            this.gameinfo.setMovingStrategy(new RealisticMovingStrategy());
        }
        else if ( this.gameinfo.getMovingStrategy() instanceof RealisticMovingStrategy ){
            this.gameinfo.setMovingStrategy(new SimpleMovingStrategy( ));
        }
        else {
            //Another strategy
        }
    }

    public void toggleShootingMode( ){
        this.cannon.toggleShootingMode( );
        this.gameinfo.setShootingMode(this.cannon.getShootingMode());
    }

    private class Memento {
        private int score;
        private int cannonX;
        private int cannonY;
        private int cannonPower;
        private double cannonAngle;
        private IShootingMode shootingModel;
        private IMovingStrategy movingStrategy;
        private List<AbsEnemy> enemies;
    }

    public Object createMemento( ) {
        Memento m = new Memento( );
        m.score = this.getScore();
        m.cannonX = this.getCannonPosition( ).getX( );
        m.cannonY = this.getCannonPosition( ).getY( );
        m.cannonPower = this.getCannonPower();
        m.cannonAngle = this.getCannonAngle();
        m.movingStrategy = this.gameinfo.getMovingStrategy();
        m.shootingModel = this.gameinfo.getShootingMode();
        m.enemies = new ArrayList<AbsEnemy>();
        m.enemies.addAll(this.enemies);

        return m;
    }

    public void setMemento( Object memento ) {
        Memento m = (Memento)memento;
        this.gameinfo.setScore(m.score);
        this.cannon.getPosition( ).setX( m.cannonX );
        this.cannon.getPosition( ).setY( m.cannonY );
        this.cannon.setAngle(m.cannonAngle);
        this.cannon.setPower(m.cannonPower);
        this.gameinfo.setCannonPower(m.cannonPower);
        this.gameinfo.setCannonAngle(m.cannonAngle);
        this.gameinfo.setMovingStrategy(m.movingStrategy);
        this.gameinfo.setShootingMode(m.shootingModel);
        this.cannon.setShootingMode(m.shootingModel);
        this.enemies = new ArrayList<AbsEnemy>();
        this.enemies.addAll(m.enemies);
    }

    @Override
    public void registerCommand(AbstractGameCommand cmd) {
        this.unexecuteCmds.add( cmd );
    }

    @Override
    public void undoLastCommand( ) {
        if( !this.executedCmds.isEmpty( ) ){
            AbstractGameCommand cmd = this.executedCmds.pop( );
            cmd.unExecute( );
            this.notifyObservers( );
        }
    }

}
