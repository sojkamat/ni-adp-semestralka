package cz.cvut.fit.miadp.mvcgame.decorator;

public class NeutralText1 extends TextDecorator{

    public NeutralText1(TextInterface decoratedText) {
        super(decoratedText);
    }

    @Override
    public String getText() {
        decoratedText.getText();
        return addGoodText(decoratedText.getText());
    }

    public String addGoodText(String text) {
        return text + " Hit those green pigs!";
    }
}