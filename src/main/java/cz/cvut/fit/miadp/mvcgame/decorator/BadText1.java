package cz.cvut.fit.miadp.mvcgame.decorator;

public class BadText1 extends TextDecorator{

    public BadText1(TextInterface decoratedText) {
        super(decoratedText);
    }

    @Override
    public String getText() {
        decoratedText.getText();
        return addGoodText(decoratedText.getText());
    }

    public String addGoodText(String text) {
        return text + " You suck!";
    }
}