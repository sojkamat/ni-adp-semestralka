package cz.cvut.fit.miadp.mvcgame.command;

import cz.cvut.fit.miadp.mvcgame.model.IGameModel;

public class ChangeTextCmd extends AbstractGameCommand {

    public ChangeTextCmd(IGameModel subject ){
        this.subject = subject;
    }

    @Override
    protected void execute( ) {
        this.subject.changeText();
    }
}
