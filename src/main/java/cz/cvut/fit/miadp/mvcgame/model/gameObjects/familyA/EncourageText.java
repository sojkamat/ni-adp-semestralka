package cz.cvut.fit.miadp.mvcgame.model.gameObjects.familyA;

import cz.cvut.fit.miadp.mvcgame.abstractFactory.IGameObjectsFactory;
import cz.cvut.fit.miadp.mvcgame.model.Position;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.AbsStaticText;

public class EncourageText extends AbsStaticText {

    private IGameObjectsFactory goFact;

    public EncourageText(Position initialPosition, IGameObjectsFactory goFact) {
        this.position = initialPosition;
        this.goFact = goFact;
        this.text = "Yaaay, c'mon!";
    }

    @Override
    public String getText() {
        return this.text;
    }

}
