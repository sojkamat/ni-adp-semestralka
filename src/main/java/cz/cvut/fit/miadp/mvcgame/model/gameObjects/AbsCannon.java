package cz.cvut.fit.miadp.mvcgame.model.gameObjects;

import java.util.List;

import cz.cvut.fit.miadp.mvcgame.state.DoubleShootingMode;
import cz.cvut.fit.miadp.mvcgame.state.IShootingMode;
import cz.cvut.fit.miadp.mvcgame.state.ShotgunShootingMode;
import cz.cvut.fit.miadp.mvcgame.state.SingleShootingMode;
import cz.cvut.fit.miadp.mvcgame.visitor.IVisitor;

public abstract class AbsCannon extends GameObject {

    protected IShootingMode shootingMode;
    protected double angle;
    protected int power;
    protected static IShootingMode SINGLE_SHOOTING_MODE = new SingleShootingMode( );
    protected static IShootingMode DOUBLE_SHOOTING_MODE = new DoubleShootingMode( );
    protected static IShootingMode SHOTGUN_SHOOTING_MODE = new ShotgunShootingMode( );

    public abstract void moveUp( );
    public abstract void moveDown( );
    public abstract void aimUp( );
    public abstract void aimDown( );
    public abstract void powerUp( );
    public abstract void powerDown( );

    public abstract List<AbsMissile> shoot( );
    public abstract void primitiveShoot( );

    @Override
    public void acceptVisitor( IVisitor visitor ){
        visitor.visitCannon( this );
    }

    public void toggleShootingMode( ){
        if ( this.shootingMode instanceof SingleShootingMode ){
            this.shootingMode = DOUBLE_SHOOTING_MODE;
        }
        else if ( this.shootingMode instanceof DoubleShootingMode ){
            this.shootingMode = SHOTGUN_SHOOTING_MODE;
        }
        else if ( this.shootingMode instanceof ShotgunShootingMode ){
            this.shootingMode = SINGLE_SHOOTING_MODE;
        }
    }

    public IShootingMode getShootingMode() {
        return this.shootingMode;
    }

    public void setShootingMode(IShootingMode shootingMode) {
        this.shootingMode = shootingMode;
    }

    public void setPower(int power) {
        this.power = power;
    }

    public int getPower() {
        return power;
    }

    public void setAngle(double angle) {
        this.angle = angle;
    }

    public double getAngle() {
        return angle;
    }
}
