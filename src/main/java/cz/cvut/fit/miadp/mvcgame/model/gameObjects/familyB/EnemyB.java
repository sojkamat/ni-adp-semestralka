package cz.cvut.fit.miadp.mvcgame.model.gameObjects.familyB;

import cz.cvut.fit.miadp.mvcgame.abstractFactory.IGameObjectsFactory;
import cz.cvut.fit.miadp.mvcgame.model.Position;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.AbsEnemy;

public class EnemyB extends AbsEnemy {

    private IGameObjectsFactory goFact;

    public EnemyB(Position initialPosition, IGameObjectsFactory goFact ) {
        this.position = initialPosition;
        this.goFact = goFact;
        this.destroyed = Boolean.FALSE;
        this.points = 3;
    }

}

