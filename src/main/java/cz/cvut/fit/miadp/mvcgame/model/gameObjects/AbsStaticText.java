package cz.cvut.fit.miadp.mvcgame.model.gameObjects;

import cz.cvut.fit.miadp.mvcgame.decorator.TextInterface;
import cz.cvut.fit.miadp.mvcgame.visitor.IVisitor;

public abstract class AbsStaticText extends GameObject implements TextInterface {

    protected String text;

    public void setText(String text) {
        this.text = text;
    }

    @Override
    public void acceptVisitor( IVisitor visitor ){
        visitor.visitStaticText( this );
    }
}
