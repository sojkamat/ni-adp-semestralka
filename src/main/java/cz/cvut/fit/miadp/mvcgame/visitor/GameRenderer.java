package cz.cvut.fit.miadp.mvcgame.visitor;

import cz.cvut.fit.miadp.mvcgame.bridge.IGameGraphics;
import cz.cvut.fit.miadp.mvcgame.config.MvcGameConfig;
import cz.cvut.fit.miadp.mvcgame.decorator.BadText1;
import cz.cvut.fit.miadp.mvcgame.decorator.GoodText1;
import cz.cvut.fit.miadp.mvcgame.decorator.TextInterface;
import cz.cvut.fit.miadp.mvcgame.model.Position;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.*;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.familyA.EnemyA;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.familyA.EncourageText;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.familyB.DiscourageText;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.familyB.EnemyB;
import cz.cvut.fit.miadp.mvcgame.state.DoubleShootingMode;
import cz.cvut.fit.miadp.mvcgame.state.IShootingMode;
import cz.cvut.fit.miadp.mvcgame.state.ShotgunShootingMode;
import cz.cvut.fit.miadp.mvcgame.state.SingleShootingMode;
import cz.cvut.fit.miadp.mvcgame.strategy.IMovingStrategy;
import cz.cvut.fit.miadp.mvcgame.strategy.RealisticMovingStrategy;
import cz.cvut.fit.miadp.mvcgame.strategy.SimpleMovingStrategy;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;

import java.time.LocalDateTime;

public class GameRenderer implements IVisitor {

    private IGameGraphics gr;

    public void setGraphicContext( IGameGraphics gr ) {
        this.gr = gr;
    }

    @Override
    public void visitCannon(AbsCannon cannon) {
        this.gr.drawImage( "images/cannon.png", cannon.getPosition( ) );
    }

    @Override
    public void visitMissile(AbsMissile missile) {
        this.gr.drawImage( "images/missile.png", missile.getPosition( ) );
    }

    @Override
    public void visitEnemy(AbsEnemy enemy) {
        if (enemy instanceof EnemyA) this.gr.drawImage( "images/enemy1.png", enemy.getPosition( ) );
        else if (enemy instanceof EnemyB) this.gr.drawImage( "images/enemy2.png", enemy.getPosition( ) );
    }

    @Override
    public void visitGameinfo(AbsGameinfo gameinfo) {
        IMovingStrategy type = gameinfo.getMovingStrategy();
        IShootingMode shoot = gameinfo.getShootingMode();
        String sType = "";
        String sShoot = "";
        if (type instanceof SimpleMovingStrategy) {sType = "SIMPLE";}
        else if (type instanceof RealisticMovingStrategy) {sType = "REALISTIC";}
        if (shoot instanceof SingleShootingMode) {sShoot = "SINGLE";}
        else if (shoot instanceof DoubleShootingMode) {sShoot = "DOUBLE";}
        else if (shoot instanceof ShotgunShootingMode) {sShoot = "SHOTGUN";}

        this.gr.drawText(   "    === GAMEINFO ===" +
                                "\n_____________________________\n" +
                                "\n - SCORE: \t" + gameinfo.getScore() +
                                "\n - POWER: \t" + gameinfo.getCannonPower() +
                                "\n - ANGLE: \t" + (int)(gameinfo.getCannonAngle()*180/Math.PI*(-1))%360 +
                                "\n - TYPE: \t\t" + sType +
                                "\n - MODE: \t\t" + sShoot,
                new Position (MvcGameConfig.GAMEINFO_POS_X, MvcGameConfig.GAMEINFO_POS_Y));
    }

    @Override
    public void visitStaticText(AbsStaticText staticText) {
        String finalText = staticText.getText();
        this.gr.drawText(   finalText, new Position (MvcGameConfig.MAX_X/2, MvcGameConfig.MAX_Y-20));
    }


}
