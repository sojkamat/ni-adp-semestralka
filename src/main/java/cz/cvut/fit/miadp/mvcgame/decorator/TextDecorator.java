package cz.cvut.fit.miadp.mvcgame.decorator;

public abstract class TextDecorator implements TextInterface{

    protected TextInterface decoratedText;

    public TextDecorator(TextInterface decoratedText) { this.decoratedText = decoratedText;}

    public String getText() {return decoratedText.getText();}

}

