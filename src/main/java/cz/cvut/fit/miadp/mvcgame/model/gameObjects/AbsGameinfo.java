package cz.cvut.fit.miadp.mvcgame.model.gameObjects;

import cz.cvut.fit.miadp.mvcgame.state.IShootingMode;
import cz.cvut.fit.miadp.mvcgame.strategy.IMovingStrategy;
import cz.cvut.fit.miadp.mvcgame.visitor.IVisitor;

public abstract class AbsGameinfo extends GameObject {

    protected int score;
    protected IShootingMode shootingMode;
    protected IMovingStrategy movingStrategy;
    protected int cannonPower;
    protected double cannonAngle;

    @Override
    public void acceptVisitor( IVisitor visitor ){
        visitor.visitGameinfo( this );
    }

    public void setScore(int score) {this.score = score;}
    public int getScore() {return this.score;}

    public void setShootingMode(IShootingMode shootingMode) {this.shootingMode = shootingMode;}
    public IShootingMode getShootingMode() {return this.shootingMode;}

    public void setMovingStrategy(IMovingStrategy movingStrategy) {this.movingStrategy = movingStrategy;}
    public IMovingStrategy getMovingStrategy() {return this.movingStrategy;}

    public void setCannonAngle(double cannonAngle) {
        this.cannonAngle = cannonAngle;
    }

    public double getCannonAngle() {
        return cannonAngle;
    }

    public void setCannonPower(int cannonPower) {
        this.cannonPower = cannonPower;
    }

    public int getCannonPower() {
        return cannonPower;
    }
}
