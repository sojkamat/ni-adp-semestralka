package cz.cvut.fit.miadp.mvcgame.model.gameObjects.familyA;

import cz.cvut.fit.miadp.mvcgame.abstractFactory.IGameObjectsFactory;
import cz.cvut.fit.miadp.mvcgame.model.Position;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.AbsEnemy;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.AbsGameinfo;
import cz.cvut.fit.miadp.mvcgame.state.IShootingMode;
import cz.cvut.fit.miadp.mvcgame.strategy.IMovingStrategy;

public class GameinfoA extends AbsGameinfo {

    private IGameObjectsFactory goFact;

//    public GameinfoA(int score, IShootingMode shootingMode, IMovingStrategy movingStrategy) {
//        this.score = score;
//        this.shootingMode = shootingMode;
//        this.movingStrategy = movingStrategy;
//    }

    public GameinfoA(Position initialPosition, IGameObjectsFactory goFact) {
        this.position = initialPosition;
        this.goFact = goFact;
    }

}
