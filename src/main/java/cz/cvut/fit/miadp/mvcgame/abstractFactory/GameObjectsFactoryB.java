package cz.cvut.fit.miadp.mvcgame.abstractFactory;

import cz.cvut.fit.miadp.mvcgame.config.MvcGameConfig;
import cz.cvut.fit.miadp.mvcgame.model.IGameModel;
import cz.cvut.fit.miadp.mvcgame.model.Position;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.*;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.familyA.EncourageText;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.familyA.EnemyA;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.familyB.DiscourageText;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.familyB.EnemyB;

public class GameObjectsFactoryB implements IGameObjectsFactory {

    private IGameModel model;

    public GameObjectsFactoryB(IGameModel model ){
        this.model = model;
    }

    @Override
    public AbsCannon createCannon() {
        return null;
    }

    @Override
    public AbsMissile createMissile(double initAngle, int initVelocity) {
        return null;
    }

    @Override
    public EnemyB createEnemy( ) {
        return new EnemyB( new Position( rng(MvcGameConfig.CANNON_POS_X + 100, MvcGameConfig.MAX_X-50),
                rng(50, MvcGameConfig.MAX_Y-50) ), this );
    }

    public int rng(int min, int max) {
        return (int) ((Math.random() * (max - min)) + min);
    }

    @Override
    public AbsGameinfo createGameinfo() {
        return null;
    }

    @Override
    public DiscourageText createStaticText() {
        return new DiscourageText( new Position(MvcGameConfig.MAX_X/2, MvcGameConfig.MAX_Y-20), this);
    }
}
