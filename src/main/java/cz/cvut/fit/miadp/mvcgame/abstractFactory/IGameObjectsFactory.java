package cz.cvut.fit.miadp.mvcgame.abstractFactory;

import cz.cvut.fit.miadp.mvcgame.model.gameObjects.*;

public interface IGameObjectsFactory {
    AbsCannon createCannon( );
    AbsMissile createMissile( double initAngle, int initVelocity );
    AbsEnemy createEnemy( );
    AbsGameinfo createGameinfo();
    AbsStaticText createStaticText();
    
}
